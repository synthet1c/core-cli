module.exports = {
  prodcatalogue: {
    repo: 'https://github.com/synthet1c/core-blog.git',
    repoName: 'core-blog',
    path: 'modules/catalogue',
    componentsPath: 'modules/catalogue/components',
    components: {
      'core-component':  'https://github.com/synthet1c/core-component.git'
    }
  },
  blogs: {
    repo: 'https://github.com/synthet1c/core-blog.git',
    repoName: 'core-blog',
    path: 'modules/blog',
    componentsPath: 'modules/blog/components',
    components: {
      'core-component':  'https://github.com/synthet1c/core-component.git'
    }
  }
}
